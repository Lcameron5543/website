# Site Web de Balle Phénix
![Image Alt](https://gitlab.com/ballephenix/website/raw/master/images/phenix_logo.png)


## introduction

Vous trouverez le code source du site web pour les Phénix ici.  Le site web
en lui-même est accessible au http://ballephenix.com ou au
https://ballephenix.gitlab.io/website/.

### Structure du site web.

Premièrement, mille excuses pour mes choix hétéroclites.  J'ai décidé d'explorer
de nouvels technologies avec ce projet.  En conséquence, le code est un peu
difficile à comprendre à première vue mais il est __tellement__ plus cool!


#### Choix

*  mithril.js comme framework dans le fureteur
*  require.js pour les dépendances
*  uikit avec sass comme framework css
*  gulp pour la compilation
*  gitlab pour le deployment

---

# English Version

All the source code is in english.  The source code is for a static web
side for The  softball league 'Balle Phénix'.

##  How to build

    $ npm install
    $ node_modules/.bin/gulp help

#  How it is made

*  mithril.js as client side framework
*  uikit as css framework
*  coffee and love
