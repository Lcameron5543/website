({
    baseUrl: "script",
    paths: {
        m: '../node_modules/mithril/mithril.min',
        _: '../node_modules/lodash/lodash.min',
        spritz: '../node_modules/spritzjs/dist/spritz',
        requireLib: '../node_modules/requirejs/require'
    },
    include: 'main',
    name: "../node_modules/almond/almond",
    out: "main-built.js",
    optimize: 'none'
})
