[title]
innerMarkdown = """
## Virée au Bar L’Astral 2000
"""

[meta]
innerMarkdown = """
Samedi 13 Juillet 2019
"""

[body]
innerMarkdown = """
La ligue de balle lente Les Phénix remercie son commanditaire Le Bar L’Astral 2000 pour sa contribution à notre saison 2019. Sa commandite a permis, en partie, à fournir 5 nouveaux uniformes d’équipes aux Phénix.

Maintenant que le beau temps se montre il sera de coutume d’aller faire un tour au Bar L’Astral 2000 le jeudi soir après nos matchs, question de socialiser entre nous et chanter, car c’est aussi un bar karaoké! Rendez-vous au [1845 Rue Ontario Est](https://goo.gl/maps/L3gdVVhKvrrgYDcc7).


[![astral 2000](images/Astral_logo.png)](https://www.facebook.com/Astral-2000-494597793884464/)

<p style="text-align: center; width: 100%;">
   <a href="https://www.facebook.com/Astral-2000-494597793884464/">
       <i class="fa fa-facebook-square" style="font-size: 3em;"></i>
   </a>
</p>
"""

[smallBody]
innerMarkdown = """

Vous êtes tannés de l’hiver et vous avez hâte de sortir gant et bâton, de jouer dans le sable et sur l’herbe du terrain, retrouver une ambiance amicale et se faire de nouveaux ami(e)s ?

**[lire la suite ...](#/nouvelles)**
"""
