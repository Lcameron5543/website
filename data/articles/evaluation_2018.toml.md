
[title]
innerMarkdown = """
## Evaluation et pratique 2018!
"""

[meta]
innerMarkdown = """
Lundi 07 mai 2018
"""

[body]
innerMarkdown = """
Bonsoir à tous les membres des Phénix - nouveaux et anciens,

La saison va bientôt commencer et nous souhaitons évaluer les nouveaux membres des Phénix dans un contexte de jeu. Ce qui veut dire que nous voulons voir les joueurs en action. Nous vous invitons donc à participer à cet événement le 12 mai prochain à compter de 13h30 au parc Walter-Stewart. Apportez votre gant !

Pour les anciens, venez faire connaissance avec les nouveaux, et par le fait même, vous mettre en forme pour la prochaine saison. Tous les anciens sont bienvenus.

Les capitaines seront rapidement appelés à faire leurs équipes après la journée d'évaluation. Nous prévoyons commencer à jouer dès le jeudi 17 mai prochain. L'heure vous sera confirmée par vos capitaines.

Pour votre information, cette saison nous allons avoir 4 équipes de 10 joueurs et pour ceux qui connaissent des joueurs ou joueuses qui voudraient à l'occasion agir comme remplaçants, nous sommes à constituer une liste qui servira aux capitaines pour faciliter les remplacements. N'hésitez pas à nous donner les noms et adresses courriel de vos amis qui seraient intéressés.

À bientôt

"""

[smallBody]
innerMarkdown = """

Bonsoir à tous les membres des Phénix - nouveaux et anciens,

La saison va bientôt commencer et nous souhaitons évaluer les nouveaux membres des Phénix dans un contexte de jeu. Ce qui veut dire que nous voulons voir les joueurs en action. Nous vous invitons donc à participer à cet événement le 12 mai prochain à compter de 13h30 au parc Walter-Stewart. Apportez votre gant !


**[Tous les details ...](#/nouvelles)**

"""
