[title]
innerMarkdown = """
## Inscription 2016!
"""

[meta]
innerMarkdown = """
Lundi 14 mars 2016
"""

[body]
innerMarkdown = """

Bonjour à tous les phénixiens phénixiennes.

Eh bien oui l'hiver tire à ça fin et il sera bientôt temps de sortir gants et
batons (en bon français mites et battes) pour jouer à notre sport préféré:
la balle-molle.


**L'inscription aura lieu le vendredi 18 mars 2016 de 19h30 à 21h30 à la [taverne
le normandie](http://bit.ly/1pmNWu8)**; situé au [1295 rue amherst,
Montréal](http://bit.ly/1QUC6Ou). Il s'agit de l'endroit ou nous avons fêté la
fin de la dernière saison.


Le prix de l'inscription est de 110$ incluant la location du chandail au coût de
10$.  La valeur de la location du chandail vous sera rendu en fin de saison
lorsque vous le rendrez.

Pour ceux et celles qui ne pourront être présent à l'inscription, vous pouvez
vous inscrire par courrier.  Imprimez, remplissez et envoyez le formulaire
avec le chèque.


Nous avons très hâte de vous y voir et de fêter avec vous!!!


Votre comité de balle les phénix.

[<i class="fa fa-paperclip"></i> Formulaire d'inscription](/files/inscription2016.pdf)

"""

[smallBody]
innerMarkdown = """
Bonjour à tous les phénixiens phénixiennes.

Eh bien oui l'hiver tire à ça fin et il sera bientôt temps de sortir gants et
batons (en bon français mites et battes) pour jouer à notre sport préféré:
la balle-molle.


**L'inscription aura lieu le vendredi 18 mars 2016 de 19h30 à 21h30 à la [taverne
le normandie](http://bit.ly/1pmNWu8)**; situé au [1295 rue amherst,
Montréal](http://bit.ly/1QUC6Ou). Il s'agit de l'endroit ou nous avons fêté la
fin de la dernière saison.

**[lire la suite ...](#/nouvelles)**

"""
