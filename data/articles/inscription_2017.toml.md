[title]
innerMarkdown = """
## Inscription 2017!
"""

[meta]
innerMarkdown = """
Samedi 18 mars 2017
"""

[body]
innerMarkdown = """
Bonjour à tous les phénixiens phénixiennes.

Bien que subtilement, l'hiver tire à sa fin et et la saison de la balle-molle
arrive a grand pas.  Comble du bonheur, nous retrouvons notre parc, Walter-Stewart,
et gardons notre plage horaire, les jeudi soirs.

**L'inscription aura lieu le vendredi 24 mars 2017 de 18h30 à 21h30 à la [taverne
le normandie](http://bit.ly/1pmNWu8)**; situé au [1295 rue amherst,
Montréal](http://bit.ly/1QUC6Ou). Il s'agit de l'endroit ou nous avons fêté la
fin de la dernière saison.

Le prix de l'inscription est de **110$** incluant la location du chandail au coût de
10$.  La valeur de la location du chandail vous sera rendue en fin de saison
lorsque vous le rendrez.

Pour ceux et celles qui ne pourront être présent à l'inscription, vous pouvez
vous inscrire par courrier.  Imprimez, remplissez et envoyez le formulaire
avec le chèque.

Nous allons jouer une **partie hors concours le 4 mai** et la **saison
commencera le jeudi 11 mai**.  

Nous avons très hâte de vous y voir et de fêter avec vous!!!


Votre comité de balle les phénix.

[<i class="fa fa-paperclip"></i> Formulaire d'inscription](/files/inscription2017.pdf)

"""

[smallBody]
innerMarkdown = """
Bonjour à tous les phénixiens phénixiennes.

Bien que subtilement, l'hiver tire à sa fin et et la saison de la balle-molle
arrive a grand pas.  Comble du bonheur, nous retrouvons notre parc, Walter-Stewart,
et gardons notre plage horaire, les jeudi soirs.

**L'inscription aura lieu le vendredi 24 mars 2017 de 18h30 à 21h30 à la [taverne
le normandie](http://bit.ly/1pmNWu8)**; situé au [1295 rue amherst,
Montréal](http://bit.ly/1QUC6Ou). Il s'agit de l'endroit ou nous avons fêté la
fin de la dernière saison.


**[lire la suite ...](#/nouvelles)**

"""
