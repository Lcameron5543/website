[title]
innerMarkdown = """
## Inscription 2018!
"""

[meta]
innerMarkdown = """
Samedi 17 mars 2018
"""

[body]
innerMarkdown = """
Bonjour à tous les membres de la Ligue de balle des Phénix et les autres qui
se joindront à nous,

L'hiver tire à sa fin. C'est donc un signe que la saison de la balle-molle
arrive à grand pas.

Mais d'abord, nous voudrions remercier Nathalie Ste-Marie et Sylvain Grenier
pour tout le travail effectué, la grande collaboration et la transmission des
effets de la Ligue ainsi que de tout le matériel. Nous savons que votre
implication immense a permis à la Ligue de continuer à se développer.
Merci encore.

Comme par le passé, il y aura une soirée d'inscription. Elle aura lieu le
vendredi 6 avril 2018 de 18h à 21h au [Bar l'Astral 2000](http://bit.ly/2FHSjMC)
situé au 1845 Rue Ontario E, Montréal, QC H2K 1T7. Comme vous le constatez,
c'est un nouveau lieu de rassemblement pour les Phénix. Le Bar est situé
beaucoup plus près de notre terrain de balle habituel ([parc Walter-Stuart](http://bit.ly/2G2fhxn)) que la Taverne Normandie
où nous allions auparavant et que nous avons délaissée l'an passé. Le Bar
l'[Astral 2000](http://bit.ly/2FHSjMC) est aussi notre commanditaire. Il faudra l'encourager, ce
que je ne doute même pas. Nous en profiterons pour vous parler des nouvelles
règles qui s'appliqueront cet été.

Le prix de l'inscription reste le même que l'année dernière soit de 110$
incluant la location du chandail au coût de 10$.  La valeur de la location
du chandail vous sera rendue lorsque vous le remettrez à la  fin de la saison.
De plus, nous offrirons un rabais sur l'inscription de 25 $ pour les personnes
qui seront choisies comme capitaine. Cette somme leur sera remise à la fin de
la saison. Nous jouerons les jeudis comme les années passées. La saison va
débuter au début du mois de mai quand les terrains seront officiellement
disponibles et se terminer à la fin août.

Une nouveauté cette année, il n'y aura que deux matchs par soir, ce qui
devrait nous permettre de socialiser un peu plus après les parties. Ça nous
permettra d'utiliser l'Astral comme lieu de rassemblement pour rire un peu
et nous rappeler nos bons et mauvais coups.    

Pour ceux et celles qui ne pourront être présents à l'inscription, vous
pouvez vous inscrire par courrier. Imprimez, remplissez et envoyez le
formulaire d'inscription et le code de conduite avec votre chèque. Faites-vite,
puisque les places sont limitées. Maximum 50 joueurs.

Nous avons très hâte de vous voir !!!

Votre comité exécutif de La Ligue de balle les Phénix.

Marie-Ève, Didier, Richard et Gilles

[<i class="fa fa-paperclip"></i> Formulaire d'inscription](/files/inscription2018.pdf)
[<i class="fa fa-paperclip"></i> Code de conduite](/files/code_de_conduite.pdf)

"""

[smallBody]
innerMarkdown = """

Bonjour a tous!

**L'inscription pour la saison 2018 aura lieu le vendredi 6 avril 2018 de 18h à
21h au [Bar l'Astral 2000](http://bit.ly/2FHSjMC)**.  Le prix reste le meme a
110$ (incluant un 10$ qui de depot pour le chandail).

Cette annee, il vous faudra remplir le
[formulaire d'inscription](/files/inscription2018.pdf) et un signer un
[code de conduite](/files/code_de_conduite.pdf).  Si vous ne pouvez pas
vous presenter a l'inscription, vous pouvez toujours envoyer les formulaires
ainsi que votre cheque par la poste.

[<i class="fa fa-paperclip"></i> Formulaire d'inscription](/files/inscription2018.pdf)
[<i class="fa fa-paperclip"></i> Code de conduite](/files/code_de_conduite.pdf)

**[Tous les details ...](#/nouvelles)**

"""
