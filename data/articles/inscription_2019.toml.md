[title]
innerMarkdown = """
## Inscription 2019!
"""

[meta]
innerMarkdown = """
Dimanche 17 Février 2019
"""

[body]
innerMarkdown = """

Vous êtes tannés de l’hiver et vous avez hâte de sortir gant et bâton, de jouer
dans le sable et sur l’herbe du terrain, retrouver une ambiance amicale et se
faire de nouveaux ami(e)s ?

Venez rencontrer des membres de la ligue de balle-lente Les Phénix - et tous
ceux et celles qui veulent se joindre à nous - à une soirée d’inscription, le
vendredi 5 avril 2019, de 18h30 à 20h30, au Bar L’Astral 2000, situé au 1845
rue Ontario Est, à Montréal. Ce bar qui nous commandite permet de nous réunir
après nos parties pour parler, boire et chanter au karaoké!

Combien ça coûte pour jouer? Les frais d’inscription 2019 sont de 100$. Aucun
dépôt pour le chandail d’équipe. Payable en argent comptant ou chèque. Saison
et party compris.

Chaque semaine, il y aura deux parties à l’horaire le jeudi soir au parc
Walter-Stewart, près du métro Frontenac,  de mai à fin août (environ 16
semaines de jeu). Une pratique optionnelle sera organisée le dimanche.
Les nouveaux joueurs seront conviés fin avril pour une évaluation sur le
terrain afin de permettre la formation d’équipes équilibrées.

Au plaisir de vous voir en grand nombre le 5 avril!

Les personnes qui préfèrent s’inscrire par courrier peuvent remplir et signer
le [formulaire et le code de conduite](/files/inscription_et_code_2019.pdf).

Pour information sur les inscriptions : Richard Dicaire, trésorier, au 514-714-2246

Votre comité des Phénix


[<i class="fa fa-paperclip"></i> Formulaire d'inscription](/files/inscription_et_code_2019.pdf)


"""

[smallBody]
innerMarkdown = """

Vous êtes tannés de l’hiver et vous avez hâte de sortir gant et bâton, de jouer dans le sable et sur l’herbe du terrain, retrouver une ambiance amicale et se faire de nouveaux ami(e)s ?

**[lire la suite ...](#/nouvelles)**
"""
