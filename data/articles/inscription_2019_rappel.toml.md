[title]
innerMarkdown = """
## Viens t’inscrire le 5 avril
"""

[meta]
innerMarkdown = """
Lundi 1er Avril 2019
"""

[body]
innerMarkdown = """

Profitez de l’arrivée du printemps pour vous présenter à la soirée d’inscription
la ligue de balle-lente Les Phénix le vendredi 5 avril 2019, de 18h30 à 20h30,
au [Bar L’Astral 2000, situé au 1845 rue Ontario Est, à Montréal](https://goo.gl/maps/3SND4ZqGZKP2). Coût de 100$
comptant ou chèque pour jouer le jeudi soir au parc Walter-Stewart, de la
mi-mai à la fin août. Évaluation des nouveaux vers la fin avril. Le comité
2019 se fera un plaisir de vous accueillir, répondre vos questions et
socialiser avec les futurs joueurs.

[<i class="fa fa-paperclip"></i> Formulaire d'inscription](/files/inscription_et_code_2019.pdf)


"""
