[title]
innerMarkdown="""
## Inscription 2016 et Dates!
"""

[meta]
innerMarkdown = """
Jeudi 31 mars 2016
"""


[body]
innerMarkdown = """
Bonjour a tous,

Permièrement, il est encore temps de s'inscrire.  Par ordre de l'Association de
Sport de Ball de Montréal, **l'inscription doit être fini le 15 avril**.  Donc
imprimez et envovez votre inscription le plus vite possible!

Deuxièmement, nous avons des nouvelles sur les dates.  La **saison commencera
le jeudi 12 mai**.  Nous allons jouer une **partie hors concours le 5 mai**.
Comme aux saisons précédentes, nous jouerons les jeudi soir au parc
Walter-Stewart et y organiserons une pratique les dimanches matins.

Pour les nouveaux, les **évaluations se feront dans les alentours du 30 avril**.
Il s'agit d'une journee ou les capitaines vous observerons pour savoir votre
niveau dans l'optique de creer des équipes les plus égales que possible.


Votre commite ballephenix adoré!

[<i class="fa fa-paperclip"></i> Formulaire d'inscription](/files/inscription2016.pdf)


"""



[smallBody]
innerMarkdown = """

Bonjour a tous,

Permièrement, il est encore temps de s'inscrire.  Par ordre de l'Association de
Sport de Ball de Montréal, **l'inscription doit être fini le 15 avril**.  Donc
imprimez et envovez votre inscription le plus vite possible!

**[lire la suite ...](#/nouvelles)**

"""
