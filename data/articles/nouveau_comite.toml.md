[title]
innerMarkdown = """
## Nouveau Comité 2019!
"""

[meta]
innerMarkdown = """
Dimanche 17 Février 2019
"""

[body]
innerMarkdown = """
La dernière année des Phénix s’est terminée avec l’élection d’un nouveau comité
exécutif.

Les membres présents à l’assemblée générale annuelle du 2 novembre 2018 ont élu
Marc-André Bonin au poste de vice-président et Alexandre Gauthier au poste de
secrétaire. Les postes de Gilles Dicaire, président, et Richard Dicaire,
trésorier, n’étaient pas en élection. Le nouvel exécutif des Phénix tient à
remercier Marie-Ève Clavel et Didier Amyot pour leur engagement des dernières années.

"""

[smallBody]
innerMarkdown = """
La dernière année des Phénix s’est terminée avec l’élection d’un nouveau comité
exécutif.

**[lire la suite ...](#/nouvelles)**
"""
