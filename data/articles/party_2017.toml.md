[title]
innerMarkdown = """
## Party fin de saison 2017!
"""

[meta]
innerMarkdown = """
Mercredi 23 Aout 2017
"""

[body]
innerMarkdown = """
Bonjour à tous les phénixiens phénixiennes.

Les séries commencent et nous devons déjà penser a fêter la fin de
la saison.  Comme à l'habitude, nous allons festoyer à la [taverne
normandie](http://www.taverne-normandie.ca/), [1295 Amherst](https://goo.gl/maps/neafXHhkW292).
Le party aura lieu le **vendredi 8 septembre 2017**, nous devrions
commencer à arriver vers 19h00.  Il y aura pizza, biere et animation.

#  <i class="fa fa-beer" aria-hidden="true"></i>

bien à vous,

votre commite.

"""
