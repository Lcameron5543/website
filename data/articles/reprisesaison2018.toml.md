[title]
innerMarkdown = """
## Reprise saison 2018
"""

[meta]
innerMarkdown = """
Lundi 9 juillet 2018
"""

[body]
innerMarkdown = """

Bonjour a toutes et a tous!

Finalement, les travaux au parc walter-stewart sont finis et nous pouvons
reprendre nos activites.  Nous devrions finir la saison plus tard pour
reprendre nos parties.

On se voit jeudi!

"""
