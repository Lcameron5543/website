

[title]
innerMarkdown = """
## Retour à Walter-Stewart
"""

[meta]
innerMarkdown = """
Samedi 20 juin 2017
"""

[body]
innerMarkdown = """
Bonjour a tous,

nous sommes heureux de vous aviser que nous retrouverons notre nid dès jeudi!
En effet, a partir de ce jeudi (22 juin 2017) nous allons jouer nos parties
au parc Walter-Stewart, juste à cote du metro Frontenac.

Pour ceux qui sont interessés, le parc Walter-Stewart est le lieux où Rodger
Brulotte est tombé en amour avec le baseball. Walter Stewart fut un journaliste
important au canada englais, mais comme le parc fut batisé avant le déces de
M. Stewart, il serait surprenant que le parc aille été nomme en son honneur.
Il y a un tres bon restaurant tout près du parc, le Resto De La Rivière; c'est
assez cher.  Il y a, aussi, une nombre incalculable de gens qui ont perdu leurs
virginité dans notre cher parc; probablement sans penser à notre cher Rodger,
au journaliste homonyme ou d'avoir salivé à l'idee une creme-brulee.

Veuillez rependre la bonne nouvelle; dites le a vos amis, chums/blondes, beaux parents...
Merci beaucoup,

Comité ballephenix!
"""

[smallBody]
innerMarkdown = """

Bonjour a tous,

nous sommes heureux de vous aviser que nous retrouverons notre nid dès jeudi!
En effet, a partir de ce jeudi (22 juin 2017) nous allons jouer nos parties
au parc Walter-Stewart, juste à cote du metro Frontenac.

Veuillez rependre la bonne nouvelle; dites le a vos amis, chums/blondes, beaux parents...
Merci beaucoup,

**[lire la suite ...](#/nouvelles)**

"""
