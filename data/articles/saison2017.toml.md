[title]
innerMarkdown = """
## Saison 2017!
"""

[meta]
innerMarkdown = """
Samedi 6 mai 2017
"""

[body]
innerMarkdown = """

Bonjour a toutes et a tous!

Nous avons quelques problèmes avec le parc Walter-Stewart.  La ville
re-tourbera bientôt le terrain et nous n'allons pouvoir l'utiliser avant la
semaine du 19 juin.  D'ici la, nous allons etre hébergé au parc [Georges-Vernot](http://bit.ly/2qMdsdm)
dans St-Michel.

Avec tout ces délais, nous sommes obligé de commencer la saison un peu plus
tard que prévue.  La saison commencera donc le **jeudi 18 mai 2017**.  Il n'y
aura donc aucun match pré-saison.

Nous sommes toujours a la **recherche de capitaines**.  La case "je veux devenir
capitaine" ne fut pas des plus populaire.  Nous cherchons donc intensément
quatre capitaines au sang neuf pour diriger cinq équipes au sang chaud.


En résumé.

* La saison debute le **jeudi 18 mai 2017** au parc **[Georges-Vernot](http://bit.ly/2qMdsdm)**
* Nous retrouverons le parc [Walter-Stewart](http://bit.ly/2qDlnMW) la semaine du 19 juin
* Il nous manque 4 capitaines

"""

[smallBody]
innerMarkdown = """
Bonjour a toutes et a tous!

Nous avons quelques problèmes avec le parc Walter-Stewart.  La ville
re-tourbera bientôt le terrain et nous n'allons pouvoir l'utiliser avant la
semaine du 19 juin.  D'ici la, nous allons etre hébergé au parc [Georges-Vernot](http://bit.ly/2qMdsdm)
dans St-Michel.


**[lire la suite ...](#/nouvelles)**

"""
