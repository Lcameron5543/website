[title]
innerMarkdown = """
## Début Saison 2021
"""

[meta]
innerMarkdown = """
Mardi 15 Juin 2021
"""

[body]
innerMarkdown = """
Nous avons eu le permis de la ville de Montréal pour débuter notre saison le 24 Juin !!! 
Notez que la ville n'a émis aucun permis avant le 20 juin, le Jeudi 24 Juin est le plus tôt que nous pouvons débuter notre saison avec notre permis.

Comme la ville ne prépare pas de terrain et ne fournit pas d'officiel, les 24 juin et 1 juillet, ce sera des matchs hors concours avec les membres présents.

Voici donc sans plus tarder notre calendrier pour la saison 2021 !!!
Votre Comité 2021
"""

[smallBody]
innerMarkdown = """

Nous avons eu le permis de la ville de Montréal pour débuter notre saison le 24 Juin !!! 
Notez que la ville n'a émis aucun permis avant le 20 juin, le Jeudi 24 Juin est le plus tôt que nous pouvons débuter notre saison avec notre permis.

**[lire la suite ...](#/nouvelles)**
"""
