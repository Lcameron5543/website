[title]
innerMarkdown = """
## Début saison 2017 retardée
"""

[meta]
innerMarkdown = """
Samedi 23 avril 2017
"""

[body]
innerMarkdown = """
Suite à la situation au parc Walter-Stewart, nous n'avons toujours pas de date définie
pour le début de la saison. La situation devrait se régler incessamment.  Nous vous
donnerons des nouvelles dès que nous en aurons.

Merci beaucoup,

Comité ballephenix!
"""
