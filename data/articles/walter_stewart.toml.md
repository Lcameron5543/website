[title]
innerMarkdown = """
# Report de la saison 2016
"""

[meta]
innerMarkdown = """
Mercredi 11 mai 2016
"""

[body]
innerMarkdown = """
Le parc walter-stewart est présentement en réaménagement.  Conséquemment, nous
devons reporter la saison jusqu'à la fin juin, date à laquelle le parc devrait
être prêt.


Plusieurs hypothèses sont envisagées pour ne pas perdre de matchs.  Nous pensons
à jouer les mercredis et jeudis ou rallonger la saison durant le mois de
septembre.  Malheuresement, nous ne pouvons prendre une désision officielle
avant de prendre connaissance de la date exacte de réouverture du parc.

Votre comité de balle les phénix.
"""

[smallBody]
innerMarkdown = """
Le parc walter-stewart est présentement en réaménagement.  Conséquemment, nous
devons reporter la saison jusqu'à la fin juin, date à laquelle le parc devrait
être prêt.

**[lire la suite ...](#/nouvelles)**
"""
