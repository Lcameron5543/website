{
  description = "Trying nest.js";
  
  inputs = {
    nixpkgs.url = "nixpkgs/nixos-21.11";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }: 

    flake-utils.lib.eachDefaultSystem (system:
      let pkgs = nixpkgs.legacyPackages.${system}; in
      rec {
        packages = flake-utils.lib.flattenTree {
          hello = pkgs.hello;
          gitAndTools = pkgs.gitAndTools;
          nodejs = pkgs.nodejs;
        };
        defaultPackage = packages.hello;
        apps.hello = flake-utils.lib.mkApp { drv = packages.hello; };
        defaultApp = apps.hello;
        devShell = pkgs.mkShell {
          name = "Nid-JS";
          buildInputs = with pkgs; [
            nodejs
            flyway
            nodePackages.node2nix
            ];
        };
      }
    );
}
