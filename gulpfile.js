var gulp = require('gulp');
const { series } = require('gulp');
var help = require('gulp-task-listing');
var clean = require('gulp-clean');
var toml = require('gulp-toml');
var gulpSass = require('gulp-sass');
var rename = require("gulp-rename");
var yaml = require('gulp-yaml');
var template = require('gulp-template');
var wrapper = require('gulp-wrapper');
var webserver = require('gulp-webserver');
var marked = require('marked');
var spritz = require('spritzjs');
var modify = require('gulp-modify');
var _ = require('lodash');
var fs = require('fs');

exports.help = help;


exports.sass = function sass(){
    var extraSassPath = ['./node_modules/'];
    return gulp.src('./scss/*.scss')
        .pipe(gulpSass({includePaths: extraSassPath}).on('error', gulpSass.logError))
        .pipe(gulp.dest('./css'));
}

exports.clean = function clean(){
    return gulp.src(["lib/", "css/"], {read: false})
        .pipe(clean());
}

exports.compileResults = function compileResults(){
    return gulp.src('data/*.yaml')
    .pipe(yaml({safe: true}).on('error', console.log))
    .pipe(gulp.dest('./api'));
};

var encryptFile = function(message){
    var keyLengthInBytes = 128;
    var salt = process.env.SALT || 'salt';
    var stringKey = salt + (process.env.KEY || 'qwerty');
    var key = spritz.hash(stringKey, keyLengthInBytes);
    var byteArrMessage = spritz.stringToUtf8ByteArray(message);
    var cypher = spritz.encrypt(key, byteArrMessage);
    return JSON.stringify(cypher);
};

var wrapEncryptedData = function(encryptedData){
    return 'window.encryptedPlayerList = ' + encryptedData + ';';
};

exports.encryptResults = function encryptResults(){
    return gulp.src('api/*.clear.json')
    .pipe(modify({
            fileModifier: function(file, content) {
                    return wrapEncryptedData(encryptFile(content));
                }
        }))
    .pipe(rename(function(path){
        path.basename = path.basename.replace(/\.clear$/i, '');
        path.extname = '.spritz.js';
        return path;
    }))
    .pipe(gulp.dest('./api'));
};

function getAvailableYears(files) {
  var rgx = /^resultats(\d*).yaml/;
  return files
    .map((f) => rgx.exec(f))
    .filter(_.isArray)
    .map((a) => _.get(a, 1, ''))
    .filter(_.identity)
    .sort();
}

exports.templateProd = function templateProd() {
    var dataFiles = fs.readdirSync('./data');

    var data = {
      production: true,
      availableYears: JSON.stringify(getAvailableYears(dataFiles)),
      salt: process.env.SALT,
      playerListLocation: process.env.PLAYER_LIST_LOCATION}
    return gulp.src('index.tmpl.html')
        .pipe(rename("index.html"))
        .pipe(template(data))
        .pipe(gulp.dest('.'));
};

exports.templateDev = function templateDev() {
    var dataFiles = fs.readdirSync('./data');
    var data = {
      availableYears: JSON.stringify(getAvailableYears(dataFiles)),
      production: false,
      salt: 'salt',
      playerListLocation: 'api/players.spritz.js'}
    console.log(data);
    return gulp.src('index.tmpl.html')
        .pipe(rename("index.html"))
        .pipe(template(data))
        .pipe(gulp.dest('.'));
};

exports.compileArticles = function compileArticles(){

    var transformArticle = function(article){
        var isMarkdownKey = /markdown/i;
        _.each(article, function(section, sectionName){
            _.each(section, function(value, key){
                if(isMarkdownKey.test(key) && _.isString(value)){
                    section[key] = marked(value);
                }
            });
        });
        return JSON.stringify(article, null, 4);
    };

    return gulp.src('data/articles/**/*.toml.md')
        .pipe(toml({to: transformArticle}))
        .pipe(wrapper({
            header: "define(['m', '_'], function (m, _ ) {\nreturn ",
            footer: ';\n});\n'
        }))
        .pipe(rename(function(path){
            path.basename = path.basename.replace(/\.toml$/i, '');
            path.extname = '.js';
            return path;
        }))
        .pipe(gulp.dest('./script/articles/'));
};


exports.base = series(
    exports.sass,
    exports.compileResults,
    exports.compileArticles,
    exports.encryptResults

);

exports.prod = series(
    exports.base,
    exports.templateProd
);

exports.dev = series(
    exports.base,
    exports.templateDev
);
