define(['m', 'articles/saison_2021'], function (m, saison_2021) {
    var simplifyArticleStructure = function(article){
        var ret = {};
        _.each(article, function(section, sectionName){
            ret[sectionName] = m.trust(section.innerMarkdown);
        });
        return ret;
    };

    var articles = [
      saison_2021
    ];

    return _.map(articles, simplifyArticleStructure);
});
