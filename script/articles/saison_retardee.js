define(['m', '_'], function (m, _ ) {
return {
    "title": {
        "innerMarkdown": "<h2 id=\"début-saison-2017-retardée\">Début saison 2017 retardée</h2>\n"
    },
    "meta": {
        "innerMarkdown": "<p>Samedi 23 avril 2017</p>\n"
    },
    "body": {
        "innerMarkdown": "<p>Suite à la situation au parc Walter-Stewart, nous n&#39;avons toujours pas de date définie\npour le début de la saison. La situation devrait se régler incessamment.  Nous vous\ndonnerons des nouvelles dès que nous en aurons.</p>\n<p>Merci beaucoup,</p>\n<p>Comité ballephenix!</p>\n"
    }
};
});
