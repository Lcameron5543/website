requirejs.config({
    paths: {
        '_': '../node_modules/lodash/lodash.min',
        'm': '../node_modules/mithril/mithril.min',
        'spritz': '../node_modules/spritzjs/dist/spritz'
    }
});


requirejs(['m', 'views/mainPage', 'views/historicPage', 'views/newsPage', 'views/calendarPage', 'views/rulesPage', 'views/photosPage', 'views/confidentialPage', 'views/contactPage', 'views/inscriptionForm'], function(m, mainPage, historicPage, newsPage, calendarPage, rulesPage, photosPage, confidentialPage, contactPage, inscriptionForm){
    m.route.mode = "hash";
    m.route(document.body, "/", {
        "/": mainPage,
        "/nouvelles": newsPage,
        "/inscription-form": inscriptionForm,
        "/calendrier": calendarPage,
        "/calendrier/:year": calendarPage,
        "/reglement": rulesPage,
        "/photos": photosPage,
        "/membre": confidentialPage,
        "/historique": historicPage,
        "/contact": contactPage
    });
});
