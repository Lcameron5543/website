define(['m', '_'], function (m, _) {
    var teams = {};

    teams.teams = function(){
        return [
            {name: 'remplaçants', color: '#FFA500'},
            {name: 'jaune', color: '#FF0'},
            {name:'rouge', color: '#F00'},
            {name:'bleu', color: '#00F'},
            {name:'gris', color: '#808080'},
            {name:'vert', color: '#008000'},
            {name: 'noir', color: '#000000'},
            {name: 'orange', color: '#FFA500'},
            {name: 'mauve', color: '#E0B0FF'}
        ];
    };

    teams.getTeamColor = function(teamName){
        var color;
        _.each(teams.teams(), function(team){
            if(team.name == teamName){
                color = team.color;
                return false;
            }
        });
        return color;
    };

    return teams;
});
