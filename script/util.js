define(['m'], function(m) {
    var util = {};
    util.loremIpsum = function(n){
        var loremIpsum =  "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec a"+
               " diam lectus. Sed sit amet ipsum mauris. Maecenas congue ligula "+
               "ac quam viverra nec consectetur ante hendrerit. Donec et mollis"+
               " dolor. Praesent et diam eget libero egestas mattis sit amet"+
               " vitae augue. Nam tincidunt congue enim, ut porta lorem lacinia "+
               "consectetur. Donec ut libero sed arcu vehicula ultricies a non"+
               " tortor. Lorem ipsum dolor sit amet, consectetur adipiscing "+
               "elit. Aenean ut gravida lorem. Ut turpis felis, pulvinar a "+
               "semper sed, adipiscing id dolor. Pellentesque auctor nisi id "+
               "magna consequat sagittis. Curabitur dapibus enim sit amet elit "+
               "pharetra tincidunt feugiat nisl imperdiet. Ut convallis libero "+
               "in urna ultrices accumsan. Donec sed od";
        loremIpsum = loremIpsum.split(' ');
        n = n || loremIpsum.length;
        return loremIpsum.splice(0, n).join(" ");

    };

    util.formatDate = function(date){
        var monthNames = ['janvier', 'février', 'mars', 'avril', 'mai', 'juin',
                          'juillet', 'août', 'septembre', 'octobre', 'novembre',
                          'decembre'];
        return [date.getDate(),
                monthNames[date.getMonth()],
                date.getFullYear()].join(' ');

    };

    util.formatDateSmall = function(date){
        var smallMonthNames = ['janv', 'févr', 'mars', 'avr', 'mai', 'juin',
                          'juill', 'août', 'sept', 'oct', 'nov',
                          'dec'];
        return [date.getDate(),
                smallMonthNames[date.getMonth()],
                date.getFullYear() % 1000].join(' ');
    };

    util.join = function(arr, sep){
        if(!_.isArray(arr) || arr.length < 1){
            return [];
        }
        var ret = [arr.pop()];
        _.each(arr, function(elm){
            ret.push(sep);
            ret.push(elm);
        });
        return ret;
    };

    util.facebookLink = function(){
        return m('a[href="https://www.facebook.com/SportsPhenixMtl"]',
                   m('i.fa.fa-facebook-square')
               );
    };

    util.emailLink = function(){
        return m('a[href="mailto:sportsphenixmtl@gmail.com"]', 'sportsphenixmtl@gmail.com');
    };

    util.resultatsURL = function(year){
        var cacheBuster = window.Date.now();
        year = !!year ? year: util.getDefaultYear();
        return "api/resultats" + year + ".json?_=" + cacheBuster;
    };


    util.getAvailableYears = function(){
        if(window.availableYears){
            return _.clone(window.availableYears)
        }
        return [];
    };

    util.getDefaultYear = function(){
        return _.max(util.getAvailableYears());
    }


    util.getResults = function(year){
        var url = util.resultatsURL(year);
        return m.request({method: "GET", url: url})
    };

    util.getDefaultResults = function(){
        return util.getResults(util.getDefaultYear());
    };

    return util;
});
