define(['m', '_', 'util', 'resultParser', 'teams', 'views/topNavBar', 'views/footer', 'views/schedulView'], function(m, _, util, resultParser, teams, topNavBar, footer, schedulView) {
    function ppYear(year, availableYears) {
      return _.last(availableYears) == year ? "" : year;
    }

    var statsView = {};

    statsView.controller = function(year){
        var games = m.prop([]);
        return util.getResults(year)
               .then(games)
               .then(resultParser.getStats);
    };

    statsView.getStatRow = function(teamStats){
        var formatStat = function(n){
            return teamStats.gamePlayed === 0 ? '-' : n;
        };
        return m('tr', [
            m('td', teamStats.team),
            m('td', teamStats.gamePlayed),
            m('td', formatStat(teamStats.victories)),
            m('td', formatStat(teamStats.defeats)),
            m('td', formatStat(teamStats.draws)),
            m('td', formatStat(teamStats.pointsFor)),
            m('td', formatStat(teamStats.pointsAgaint)),
            m('td', formatStat(teamStats.pointsFor - teamStats.pointsAgaint)),
            m('td', formatStat(teamStats.points))
        ]);
    };

    statsView.isStatForValidTeam = function(stat){
        var teamNames = _.map(teams.teams(), 'name');
        return _.includes(teamNames, stat.team);
    }


    statsView.mainTable = function(stats){
        var orderedStats = _.reverse(_.sortBy(stats, ['points', 'pointsFor']));
        return m('table.uk-table.uk-table-hover.uk-table-striped',[
            m('thead', m('tr', [
                m('th', 'Équipes'),
                m('th', 'PJ'),
                m('th', 'V'),
                m('th', 'D'),
                m('th', 'N'),
                m('th', 'PP'),
                m('th', 'PC'),
                m('th', 'DIF'),
                m('th', 'PTS')
            ])),
            m('tbody', _(orderedStats)
                        .filter(statsView.isStatForValidTeam)
                        .map(statsView.getStatRow)
                        .value())
        ]);
    };

    statsView.legend = function(){
        var items = ["PJ : Parties joués", "V : Victoire",  "D : Défaite",
                     "N : Nul", "PP : Points pour", "PC : Points contre",
                     "DIF : Diférenciel","PTS : Points"];
        return  m('ul.uk-subnav.uk-subnav-line.uk-flex-center',[
                    _.map(items, function(itm){
                        return m('li', itm);
                    })
                ]);
    };

    statsView.view = function(stats, year){
        return  m('div.uk-grid',
                    m('div.uk-width-medium-1-1.uk-width-large-1-1.uk-container-center',[
                        m('h2', 'Classement ' + year),
                        statsView.mainTable(stats()),
                        statsView.legend()
                    ]));

    };

    var tabView = {};

    tabView._createTab = function(activeYear, availableYears, year){
      var isActive = (activeYear == year) || (activeYear == ppYear(year, availableYears));
      return m('li', {'class': isActive ? 'uk-active': ''}, //  'uk-active'
          m('a', {href: '#/calendrier/' + ppYear(year, availableYears)}, 'Saison ' + year));
    }

    tabView.view = function(vnode, isPlayoff, activeYear, availableYears){
        var createTab = _.partial(tabView._createTab, activeYear, availableYears);
        return m('div.uk-margin', m('h2', 'Calendrier ' +  ppYear(activeYear, availableYears)),
            m('ul.uk-tab', _.reverse(_.map(availableYears, createTab))));
    };

    var playoffView = {};

    playoffView.line = function(x1, y1, x2, y2){
        return m('line', {'x1': x1,
                   'y1': y1,
                   'x2': x2,
                   'y2': y2,
                    style: 'stroke:rgb(255,0,0);stroke-width:2'})
    }

    playoffView.horizontalLine = function(x1, x2, y){
        return playoffView.line(x1, y, x2, y);
    };

    playoffView.view = function(){
        var width = 600;
        var height = 600;
        var padding = 40;
        return m('div',
            m('svg', {width: width, height: height},
                playoffView.horizontalLine(padding, width/2 - padding, height/3),
                playoffView.horizontalLine(padding, width/2 - padding, 2 * height/3)
        ));
    };

    calendarPage = {};

    calendarPage.view  = function(vnode){
        var isPlayoff = window.location.hash.endsWith('playoff');
        var availableYears = util.getAvailableYears();
        var year = _.get(m.route.param(), 'year', util.getDefaultYear());
        
        var main = isPlayoff ? m.component(playoffView) : m.component(schedulView, year, availableYears);
        return [m.component(topNavBar),
                m('div.uk-grid.uk-margin-top.uk-margin-bottom',
                    m('div.uk-width-large-3-5.uk-width-medium-4-5.uk-width-small-1-1.uk-container-center',[
                        m.component(statsView, year, availableYears),
                        m.component(tabView, isPlayoff, year, availableYears),
                        main
                    ])),
                m.component(footer)];
    };

    return calendarPage;
});
