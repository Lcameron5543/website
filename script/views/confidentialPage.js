define(['m', '_', 'teams', 'views/loginPage', 'views/playersPage', 'views/topNavBar', 'views/footer'], function (m, _ , teams, loginPage, playersPage, topNavBar, footer) {

    var confidentialPage = {};

    confidentialPage.view  = function(){
        var body;
        if(_.isEmpty(localStorage.getItem('secret'))){
            body = loginPage;
        }else{
            body = playersPage;
        }
        return [
                m.component(topNavBar),
                m.component(body),
                m.component(footer)
            ];
    };

    return confidentialPage;
});
