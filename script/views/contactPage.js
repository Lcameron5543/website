define(['m', '_', 'util', 'resultParser', 'views/topNavBar', 'views/footer'], function (m, _ , util, resultParser, topNavBar, footer) {

    var contactBody = {};

    contactBody.logo = function(){
        var style = 'height: 300px; width: 300px; background-size: 300px; background-image: url(images/logosansfond.JPG);';
        return m('div.uk-cover-background.uk-container-center', {style: style});
    };

    contactBody.logoLine = function(){
        return  m('div.uk-grid.uk-margin',
                    m('div.uk-width-1-1', contactBody.logo()));
    };

    contactBody.tel = function(){
        return ' (514) 838-8950';
    };

    contactBody.address = function(){
        return m('p', ' 10454 Sylvain-Garneau', m('br'), 'Montréal, Québec H1C 0A2');
    };

    contactBody.website = function(){
        var gitlabUrl = 'https://gitlab.com/ballephenix/website';
        return m('p', ' Le site web est ouvert et accessible sur ',
                        m('a', {href: gitlabUrl}, 'gitlab.'));
    };

    contactBody.contact = function(){
        return m('div.div.uk-panel.uk-panel-box.uk-panel-box-secondary.uk-container-center.uk-margin',[
            m('table.uk-table',
                m('tr',
                    m('td.uk-text-right',  m('i.fa.fa-desktop')),
                    m('td',  util.emailLink())
                ),
                m('tr',
                    m('td.uk-text-right',  m('i.fa.fa-phone')),
                    m('td',  contactBody.tel())
                ),
                m('tr',
                    m('td.uk-text-right',  m('i.fa.fa-envelope')),
                    m('td',  contactBody.address())
                ),
                m('tr',
                    m('td.uk-text-right',  m('i.fa.fa-gitlab')),
                    m('td',  contactBody.website())
                )
            )
        ]);

    };

    contactBody.view = function(){
        return  m('div.uk-margin-top.uk-margin-bottom.uk-grid',
                    m('div.uk-width-large-2-5.uk-width-medium-4-5.uk-width-small-1-1.uk-container-center', [
                        contactBody.logoLine(),
                        contactBody.contact()
                    ])
                );
    };

    var contactPage = {};

    contactPage.view  = function(){
        return [
                m.component(topNavBar),
                m.component(contactBody),
                m.component(footer)
            ];
    };

    return contactPage;
});
