define(['m', '_', 'util'], function (m, _, util) {
    var footer = {};

    footer.wording = function(){
        return [
            m('span.uk-hidden-small', 'Ligue de balle les Phénix... pour le plaisir de jouer!'),
            m('h4.uk-visible-small', 'Ligue de balle les Phénix')
        ];
    };

    footer.view = function(){
        return  m('footer.uk-grid.ph-footer', {style: {padding: '40px'}},
                    m('div.uk-container.uk-container-center.uk-text-center', [
                        m('div.uk-panel', footer.wording()),
                        m('ul.uk-subnav.uk-subnav-line.uk-flex-center',
                            m('li', util.emailLink())),
                        m('h1', util.facebookLink())
                    ])
        );
    };

    return footer;
});
