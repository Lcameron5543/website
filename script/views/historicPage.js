define(['m', '_', 'util', 'views/topNavBar', 'views/calendarPage'], function(m, _, util, topNavBar, calendarPage) {

    var yearSelector = {};

    yearSelector.view = function(){
        return m('div.uk-grid',
                    m('div.uk-width-large-3-5.uk-width-medium-1-1.uk-container-center',
                        yearSelector.tab()));
    };

    yearSelector.tab = function(){
        return m('ul.uk-tab.uk-margin-top', [
            m('li.uk-active', m('a', 'Saison 2016'))
        ]);
    };

    var historicPage = {};

    historicPage.view = function(){
        return [
            m.component(topNavBar),
            m.component(yearSelector)
            // m.component(calendarPage)
        ];
    };

    return historicPage;
});
