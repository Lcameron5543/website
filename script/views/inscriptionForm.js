define(['m', '_',], function (m, _ ) {

    var inscriptionForm = {};

    inscriptionForm.title = function(){
        return m('h1.uk-width-1-1.uk-text-center', 'Inscription Balle Phénix');
    };

    inscriptionForm._field = function(label){
        return m('label', label, m('input.uk-form-width-large', {type: 'text'}));
    };

    inscriptionForm.identification = function(){
        return  m('form.uk-form', m('fieldset',
                    m('legend', 'Identification'),
                    m('div.uk-margin-top.uk-margin-bottom.uk-grid',
                        m('div.uk-width-1-2', inscriptionForm._field('Nom')),
                        m('div.uk-width-1-2', inscriptionForm._field('Prénom'))),
                    m('div.uk-margin-top.uk-margin-bottom.uk-grid',
                        m('div.uk-width-1-2', inscriptionForm._field('Adresse')),
                        m('div.uk-width-1-2', inscriptionForm._field('Chanson préférée'))),
                    m('div.uk-margin-top.uk-margin-bottom.uk-grid',
                        m('div.uk-width-1-3', inscriptionForm._field('Ville')),
                        m('div.uk-width-1-3', inscriptionForm._field('Code postal')),
                        m('div.uk-width-1-3', inscriptionForm._field('Tél')))

                    ));
    };

    inscriptionForm.inscription = function(){
        return  m('form.uk-form',
                    m('h2.uk-width-1-1.uk-text-center', 'Inscription'),
                    m('div.uk-margin-top.uk-margin-bottom.uk-grid',
                        m('div.uk-width-1-1',
                            inscriptionForm._field('Inscription en duo. Inscrivez le nom du joueur')))


                    );
    };


    inscriptionForm.view  = function(){
        return [
            inscriptionForm.title(),
            inscriptionForm.identification(),
            inscriptionForm.inscription()

        ];
        // return m('form', m('label', 'Nom', m('input', {type: 'text'})));
    };

    return inscriptionForm;
});
