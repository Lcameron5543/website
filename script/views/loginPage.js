define(['m', '_'], function (m, _) {

    var loginPage = {};
    loginPage.salt = window.salt;

    loginPage.controller = function(){
        return {email: m.prop(''), password: m.prop('')};
    };

    loginPage.logo = function(){
        var style = 'height: 250px; width: 250px; background-size: 250px; background-image: url(images/logosansfond.JPG);';
        return m('div.uk-cover-background.uk-container-center', {style: style});
    };

    loginPage.logoLine = function(){
        return  m('div.uk-grid.uk-margin',
                    m('div.uk-width-1-1', loginPage.logo()));
    };

    loginPage.login = function(ctrl){
        return function(){
            localStorage.setItem('secret', loginPage.salt + ctrl.email() + ctrl.password());
            return false;
        };
    };

    loginPage.formChange = function(ctrl){
        return function(evt){
            ctrl[evt.target.name](evt.target.value);
        };
    };

    loginPage.loginForm = function(ctrl){
        return  m('div.uk-panel.uk-panel-box.uk-panel-box-secondary.uk-width-large-1-3.uk-container-center.uk-margin', [
            m('form.uk-text-center[action]', {onsubmit: loginPage.login(ctrl), onchange: loginPage.formChange(ctrl)},
                m('div.uk-form-row',
                    m('label', "Nom d'utilisateur ",
                        m('input[type=text][name=email][required].uk-form-width-small', {value: ctrl.email()} ))),
                m('div.uk-form-row',
                    m('label', "Mot de passe ",
                        m('input[type=password][name=password][required].uk-form-width-small', {value: ctrl.password()}))),
                m('div.uk-form-row', m('button.uk-button', m("i.fa.fa-mars-double"), " Pénétrez"))
            )
        ]);
    };

    loginPage.view = function(ctrl){
        return m('div', [
            loginPage.logoLine(),
            loginPage.loginForm(ctrl)
        ]);
    };

    return loginPage;
});
