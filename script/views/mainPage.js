define(['m', '_', 'views/nextGameDaySchedule', 'views/topNavBar', 'views/footer', 'articles/articles'], function (m, _ , nextGameDaySchedule, topNavBar, footer, articles) {
    var mainPageLogo = {};

    mainPageLogo.view = function(){
        return m('div.uk-grid', [
            m('div.uk-large-1-3.uk-medium-2-3.uk-container-center', [
                m('div.uk-cover-background.uk-container-center.uk-margin-left.uk-margin-top',{style: 'height:300px; width: 300px; background-size: 300px; background-image: url(images/logosansfond.JPG);'})
            ])
        ]);
    };

    var mainPageNews = {};

    mainPageNews.showMiniArticle = function(article){
      return m('div', m('h3.uk-panel-title', m('a.uk-link-muted', {href: '#/nouvelles'}, article.title)),
                      m('p', article.smallBody || article.body));
    }

    mainPageNews.view = function(){
        return m('div.uk-panel.uk-panel-box',[
            m('div.uk-panel-badge.uk-badge', [
                m('i.fa.fa-newspaper-o', {style: {'font-size': '1.7em'}})
            ]),
            mainPageNews.showMiniArticle(_.get(articles, 0, {}))
        ]);
    };

    

    var scheduleNotYetSet = {};

    scheduleNotYetSet.view = function(){
            return m('div',
                        m('h4', 'À venir...'),
                        m('p', "Suite a la situation au parc Walter-Stewart, " +
                               "nous n'avons toujours pas de date définie " +
                               "pour le début de la saison.  La situation" +
                               "devrait se regler incessamment"));
    };

    var mainPageSchedule = {};

    mainPageSchedule.view = function(){
        return m('div.uk-panel.uk-panel-box',[
            m('div.uk-panel-badge.uk-badge', m('i.fa.fa-calendar', {style: {'font-size': '1.7em'}})),
            m('h3.uk-panel-title', m('a.uk-link-muted', {href: '#/calendrier'}, 'Calendrier')),
            m.component(nextGameDaySchedule)
        ]);
    };

    var mainMap = {};

    mainMap.getWalterStewartMap = function(){
        var iframeUrl = "//www.openstreetmap.org/export/embed.html?bbox=-73.55936229228975%2C45.53102667048665%2C-73.55230271816255%2C45.53444249585723&amp;layer=mapnik"; // "//www.openstreetmap.org/export/embed.html?bbox=-73.56355190277101%2C45.5294483312637%2C-73.54943275451662%2C45.53627996625677&amp;layer=mapnik"; // "//www.openstreetmap.org/export/embed.html?bbox=-73.56355190277101%2C45.5294483312637%2C-73.54943275451662%2C45.53627996625677&amp;layer=mapnik";
        var externalLink = "http://www.openstreetmap.org/#map=17/45.53216/-73.55692";
        var iframeParams = {
            style: {border: '1px solid black', 'min-height': '300px'},
            width: '100%',
            frameborder: 0,
            scrolling: 'no',
            marginheight: 0,
            src: iframeUrl
        };
        return [
            m('iframe', iframeParams),
            m('br'),
            m('small', m('a', {href: externalLink}, 'Voir la carte'))
        ];
    };

    mainMap.getGeorgesVernotMap = function(){
// " style="border: 1px solid black"></iframe><br/><small><a href="http://www.openstreetmap.org/#map=16/45.5670/-73.6177">View Larger Map</a></small>

        var iframeUrl = "//www.openstreetmap.org/export/embed.html?bbox=-73.63185167312623%2C45.560217957150535%2C-73.60361337661745%2C45.57387292040785&amp;layer=mapnik";
        var externalLink = "http://www.openstreetmap.org/#map=17/45.53216/-73.55692";
        var iframeParams = {
            style: {border: '1px solid black', 'min-height': '300px'},
            width: '100%',
            frameborder: 0,
            scrolling: 'no',
            marginheight: 0,
            src: iframeUrl
        };
        return [
            m('iframe', iframeParams),
            m('br'),
            m('small', m('a', {href: externalLink}, 'Voir la carte'))
        ];
    };

    mainMap.view = function(){
        return m('div.uk-panel.uk-panel-box',[
            m('div.uk-panel-badge.uk-badge', [
                m('i.fa.fa-map-o', {style: {'font-size': '1.7em'}})
            ]),
            m('h3.uk-panel-title', 'Parc Walter-Stewart'),
            mainMap.getWalterStewartMap()
        ]);
    };

    var mainPageBottom = {};

    mainPageBottom.regularSizeView = function(){
        return m('div.uk-grid.uk-visible-large.uk-grid-match', [
            m('div.uk-width-large-1-3.uk-width-medium-1-3', m.component(mainPageNews)),
            m('div.uk-width-large-1-3.uk-width-medium-1-3', m.component(mainPageSchedule)),
            m('div.uk-width-large-1-3.uk-width-medium-1-3', m.component(mainMap))
        ]);
    };

    mainPageBottom.mediumSizeView = function(){
        return  m('div.uk-visible-medium', [
                    m('div.uk-grid.uk-grid-match', [
                        m('div.uk-width-medium-1-2', [m.component(mainPageNews)]),
                        m('div.uk-width-medium-1-2', [m.component(mainPageSchedule)]),
                    ]),
                    m('div.uk-grid',
                        m('div.uk-width-medium-1-1', m.component(mainMap))
                    )
        ]);
    };

    mainPageBottom.smallSizeView = function(){
        return  m('div.uk-visible-small',
                  [
                    m('div.uk-grid', m('div.uk-width-small-1-1', m.component(mainPageNews))),
                    m('div.uk-grid', m('div.uk-width-small-1-1', m.component(mainPageSchedule))),
                    m('div.uk-grid', m('div.uk-width-small-1-1', m.component(mainMap)))
              ]);
    };

    mainPageBottom.view = function(){
        return  m('div.uk-margin-top.uk-margin-bottom.uk-grid',
                    m('div.uk-width-large-3-5.uk-width-medium-4-5.uk-width-small-1-1.uk-container-center', [
                        mainPageBottom.regularSizeView(),
                        mainPageBottom.mediumSizeView(),
                        mainPageBottom.smallSizeView()
                    ])
        );
    };

    var mainPage = {};

    mainPage.view  = function(){
        return [
                m.component(topNavBar),
                m.component(mainPageLogo),
                m.component(mainPageBottom),
                m.component(footer)
            ];
    };

    return mainPage;
});
