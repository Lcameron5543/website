define(['m', '_', 'util', 'resultParser', 'articles/articles', 'views/topNavBar', 'views/footer'], function (m, _ , util, resultParser, articles, topNavBar, footer) {

    var newsPageBody = {};

    newsPageBody.logo = function(){
        var style = 'height: 250px; width: 250px; background-size: 250px; background-image: url(images/logosansfond.JPG);';
        return m('div.uk-cover-background.uk-container-center', {style: style});
    };

    newsPageBody.getArticle = function(article){
        return m('article.uk-article.uk-margin-',
            m('h1.uk-article-title', article.title),
            m('p.uk-article-meta', article.meta),
            article.body
        );
    };

    newsPageBody.view = function(){
        var arts = _.reverse(_.map(articles, newsPageBody.getArticle));
        return m('div', util.join(arts, m('hr')));
    };

    var newsPage = {};

    newsPage.view  = function(){
        return [
                m.component(topNavBar),
                m('div.uk-grid.uk-margin-top.uk-margin-bottom',
                    m('div.uk-width-large-2-5.uk-width-medium-3-5.uk-width-small-1-1.uk-container-center',
                        m.component(newsPageBody))),
                m.component(footer)
            ];
    };

    return newsPage;
});
