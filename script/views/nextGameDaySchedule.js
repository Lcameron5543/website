define(['m', '_', 'util', 'resultParser', 'views/scheduleTeamTags', 'images/rain.svg'], function (m, _ , util, resultParser, scheduleTeamTags, rainSvg) {

    var nextGameDaySchedule = {};

    nextGameDaySchedule._toDay = function(){
        var now = new Date();
        return new Date(now.getFullYear(), now.getMonth(), now.getDate());
    };

    nextGameDaySchedule.controller = function(){
        var games = m.prop([]);
        return util.getDefaultResults()
               .then(games)
               .then(resultParser.getNightGames)
               .then(function(nightGames){
                   return nextGameDaySchedule._getNextNightGame(nightGames, nextGameDaySchedule._toDay()).games;
               });
    };

    nextGameDaySchedule._getNextNightGame = function(games, date){
        return _.first(_.filter(games, function(game){
            return game.jsDate.getTime() >= date.getTime();
        }));
    };

    nextGameDaySchedule._getTableHead = function(){
        return m('thead', m('tr', [
                    m('td', 'Date'),
                    m('td', 'Heure'),
                    m('td', 'Receveur'),
                    m('td', 'Visiteur')
                ]));
    };

    nextGameDaySchedule._responsoveText = function(largeText, smallText, game){
        var ret = [];
        if(game.rain){
            ret.push(rainSvg('1.5em'));
            ret.push(' ');
        } else if(game.practice){
            ret.push(m('span.uk-badge.uk-badge-success', 'pratique!'));
            ret.push(' ');
        }
        ret.push(m('span.uk-visible-large', largeText));
        ret.push(m('span.uk-hidden-large', smallText));
        return ret;
    };

    nextGameDaySchedule._getGamesRow = function(game){
        return m('tr', [
            m('td', nextGameDaySchedule._responsoveText(util.formatDate(game.jsDate),
                                                        util.formatDateSmall(game.jsDate),
                                                        game)),
            m('td', game.time),
            m('td', scheduleTeamTags.getHomeTeamTag(game)),
            m('td',scheduleTeamTags.getAwayTeamTag(game))
        ]);
    };

    nextGameDaySchedule._getGamesRows = function(games){
        return _.map(games(), nextGameDaySchedule._getGamesRow);
    };

    nextGameDaySchedule.view = function(games){
        return  m('table.uk-table.uk-table-hover.uk-table-striped.uk-table-condensed',[
                    nextGameDaySchedule._getTableHead(),
                    m('tbody',
                        nextGameDaySchedule._getGamesRows(games)
                    )]);
    };

    return nextGameDaySchedule;
});
