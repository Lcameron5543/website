define(['m', '_', 'util', 'resultParser', 'views/topNavBar', 'views/footer'], function (m, _ , util, resultParser, topNavBar, footer) {

    var rulesPageBody = {};

    rulesPageBody.logo = function(){
        var style = 'height: 250px; width: 250px; background-size: 250px; background-image: url(images/logosansfond.JPG);';
        return m('div.uk-cover-background.uk-container-center', {style: style});
    };

    rulesPageBody.view = function(){
        return  m('div.uk-grid',
                    m('div.uk-width-1-1', rulesPageBody.logo()));
    };

    var photosPage = {};

    photosPage.view  = function(){
        return [
                m.component(topNavBar),
                m.component(rulesPageBody),
                m.component(footer)
            ];
    };

    return photosPage;
});
