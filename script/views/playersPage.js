define(['m', '_', 'spritz', 'teams'], function (m, _ , spritz, teams) {

    var playersSelectionBar = {};

    playersSelectionBar.toggleTeam = function(teamId, activatedTeams){
        return function(isActive){
            isActive = isActive != 'true';
            if(isActive){
                activatedTeams().push(teamId);
                return;
            }
            activatedTeams(_.difference(activatedTeams(), [teamId]));
        };
    };

    playersSelectionBar.teamInput = function(teamName, teamId, activatedTeams){
        var isActive = _.includes(activatedTeams(), teamId);
        var class_ =  isActive ? 'uk-badge-success' : '';
        var callback = playersSelectionBar.toggleTeam(teamId, activatedTeams);
        return  m('div.uk-badge.uk-margin-left.uk-border-rounded',
                    {class: class_, 'data-selected': isActive, onclick: m.withAttr("data-selected", callback)},
                        m('span', {style: {'min-width': '40px', display: 'inline-block'}}, teamName)
        );

    };

    playersSelectionBar.scoreInput = function(score, activatedScores){
        var isActive = _.includes(activatedScores(), score);
        var class_ =  isActive ? 'uk-badge-success' : '';
        var callback = playersSelectionBar.toggleTeam(score, activatedScores);
        return  m('div.uk-badge.uk-margin-left.uk-border-rounded',
                    {class: class_, 'data-selected': isActive, onclick: m.withAttr("data-selected", callback)},
                        m('span', {style: {'min-width': '30px', display: 'inline-block'}}, score)
        );
    };

    playersSelectionBar.getTeamInput = function(model, team){
        var label = team.name;
        return playersSelectionBar.teamInput(label, team.name, model.activatedTeams);
    };

    playersSelectionBar.chooseTeams = function(model){
        return  m('form.uk-grid', {style: {'margin-top': '1em'}},
                    m('div.uk-width-1-6', m('label', m('b', 'Équipes'))),
                    m('div.uk-width-5-6',
                        m('div.uk-grid',
                            _.map(teams.teams(), _.partial(playersSelectionBar.getTeamInput, model))
                            ))
                    );
    };

    playersSelectionBar.chooseScore = function(model){
        return  m('form.uk-grid', {style: {'margin-top': '1em'}},
                    m('div.uk-width-1-6', m('label', m('b', 'Cote'))),
                    m('div.uk-width-5-6',
                        m('div.uk-grid', [
                            playersSelectionBar.scoreInput(1, model.activatedScores),
                            playersSelectionBar.scoreInput(2, model.activatedScores),
                            playersSelectionBar.scoreInput(3, model.activatedScores),
                            playersSelectionBar.scoreInput(4, model.activatedScores),
                            playersSelectionBar.scoreInput(5, model.activatedScores)
                    ])));
    };

    playersSelectionBar.searchFactory = function(model){
        return function(){
            model.searchTerm(this.value);
        };
    };

    playersSelectionBar.searchBar = function(model){
        var icon = m('i.fa.fa-search', {'aria-hidden': true}, '');
        return m('form.uk-grid', {style: {'margin-top': '1em'}},
                    m('div.uk-width-1-1.', {style: 'text-align: right;'},
                        m('label', m('b', icon, " Rechercher ")),
                            m('input', {type: 'text', onkeypress: playersSelectionBar.searchFactory(model)})));
    };

    playersSelectionBar.controller = function(model){
        return model;
    };

    playersSelectionBar.view = function(model){
        return  m('div.uk-grid', [
                    m('div.uk-width-1-2',
                        playersSelectionBar.chooseTeams(model),
                        playersSelectionBar.chooseScore(model)),
                    m('div.uk-width-1-2',
                        playersSelectionBar.searchBar(model))
        ]);
    };


    var playersTable = {};

    playersTable._sortPlayers = function(sorting, playerList){
        var sortingKeys = _.map(sorting(), 'key');
        var sortingDirections = _.map(sorting(), function(o){
            return o.direction == 1 ? 'desc' : 'asc';
        });
        return _.orderBy(playerList, sortingKeys, sortingDirections);
    };

    playersTable.decryptPlayerList = function(encryptedPlayerList){
        var keyLengthInBytes = 128;
        var key = spritz.hash(localStorage.getItem('secret') || '', keyLengthInBytes);
        var decryptedMessage = spritz.decrypt(key, encryptedPlayerList);
        try{
            return JSON.parse(spritz.utf8ByteArrayToString(decryptedMessage));
        }catch(e){
            localStorage.removeItem('secret');
            m.route('/membre');
        }

    };

    playersTable.getEncryptedPlayerList = function(){
        var deferred = m.deferred();
        var interval = setInterval(function() {
            if(!_.isEmpty(window.encryptedPlayerList)){
                clearInterval(interval);
                deferred.resolve(window.encryptedPlayerList);
                m.redraw();
            }
        }, 333);
        return deferred.promise;
    };

    playersTable.controller = function(model){
        var ctrl = {};
        ctrl.model = model;

        ctrl.players = playersTable.getEncryptedPlayerList()
                        .then(playersTable.decryptPlayerList);

        ctrl.sortPlayers = function(){
            ctrl.players(playersTable._sortPlayers(ctrl.model.sorting, ctrl.players()));
        };
        return ctrl;
    };

    playersTable._getSortDict = function(sorting){
        var ret = {};
        _.each(sorting(), function(elm){
            ret[elm.key] = elm.direction;
        });
        return ret;

    };

    playersTable._isSortingUp = function(sorting, key){
        var dict = playersTable._getSortDict(sorting);
        return dict[key] === 1;
    };

    playersTable._isSortingDown = function(sorting, key){
        var dict = playersTable._getSortDict(sorting);
        return dict[key] === -1;
    };

    playersTable.arrows = function(sorting, key){
        var sortUp = playersTable._isSortingUp(sorting, key);
        var sortDown = playersTable._isSortingDown(sorting, key);
        var style = {
            display: 'inline-block',
            'line-height': '0.5em',
            'margin-right': '1em',
            'font-size': '0.7em'
        };
        var sortedStyle = {color: '#8cc14c', 'font-weight': 'bold'};
        return  m('span', {style: style},
                      m('i.fa.fa-chevron-up', {style: sortUp ? sortedStyle: {}}),
                      m('br'),
                      m('i.fa.fa-chevron-down',  {style: sortDown ? sortedStyle: {}})
                  );
    };

    playersTable._setSorting = function(sorting, key){
        var newDirection = {key: key, direction: -1 * playersTable._getSortDict(sorting)[key]};
        var directions = _.filter(sorting(), function(o){
            return o.key != key;
        });
        sorting([newDirection].concat(directions));
    };

    playersTable.tableHeader = function(sorting){
        var setSorting = function(key){
            return _.partial(playersTable._setSorting, sorting, key);
        };
        return  m('thead',
                    m('tr', [
                        m('th', {onclick: setSorting('team')}, playersTable.arrows(sorting, 'team'), 'Équipe'),
                        m('th', {onclick: setSorting('name')}, playersTable.arrows(sorting, 'name'), 'Nom'),
                        m('th.uk-hidden-small', 'Courriel'),
                        m('th.uk-hidden-small', 'Téléphone'),
                        m('th', {onclick: setSorting('score')}, playersTable.arrows(sorting, 'score'), 'Cote')
                    ]));
    };

    playersTable.getPlayerTeamIcon = function(player){
        var background = teams.getTeamColor(player.team);
        var style = {width: '20px', height: '20px', 'background-color': background};
        return m('div', {style: style}, m.trust('&nbsp;'));
    };

    playersTable.showOrHidePlayerDetail = function(player){
        return function(){
            player.showDetails = !player.showDetails;
        };
    };

    playersTable.playerTelLink = function(player){
        return m('a[href=tel:' + player.tel + ']', player.tel);
    };

    playersTable.playerRow = function(player){
        return  m('tr', {onclick: playersTable.showOrHidePlayerDetail(player)}, [
                    m('td', playersTable.getPlayerTeamIcon(player)),
                    m('td', player.name),
                    m('td.uk-hidden-small', player.email),
                    m('td.uk-hidden-small', player.tel),
                    m('td', player.score)
        ]);
    };

    playersTable.playerInformation = function(player){
        return  m('tr.uk-visible-small', [
                    m('td', ''),
                    m('td[colspan=3]',
                        m('ul', [
                            m('li', [m('b', 'tel: '), playersTable.playerTelLink(player)]),
                            m('li', [m('b', 'email: '), player.email])
                        ]))
                ]);
    };

    playersTable.playerRowAndInformation = function(player){
        if(player.showDetails){
            return [playersTable.playerRow(player),
                    m('tr', {style: {height: '0px'}}),
                    playersTable.playerInformation(player)];
        }
        return playersTable.playerRow(player);
    };

    playersTable.onlyPlayerOfTeams = function(visibleTeams){
        return function(player){
            return _.includes(visibleTeams, player.team);
        };
    };

    playersTable.onlyPlayerOfScore = function(visibleScores){
        return function(player){
            return _.includes(visibleScores, player.score);
        };
    };

    playersTable.playerHasBeenSearchedFor = function(searchTerm){
        return function(player){
            if(_.isEmpty(searchTerm)){
                return true;
            }
            return new RegExp(searchTerm, 'i').test(player.name);

        };
    }

    playersTable.tableBody = function(players, visibleTeams, activatedScores, searchTerm){
        return  m('tbody',
                    _(players)
                    .filter(_.isObject)
                    .filter(playersTable.onlyPlayerOfTeams(visibleTeams))
                    .filter(playersTable.onlyPlayerOfScore(activatedScores))
                    .filter(playersTable.playerHasBeenSearchedFor(searchTerm))
                    .map(playersTable.playerRowAndInformation).flatten().value()
                );
    };

    playersTable.view = function(ctrl){
        ctrl.sortPlayers();
        return  m('table.uk-table.uk-table-hover.uk-table-striped',
                    playersTable.tableHeader(ctrl.model.sorting),
                    playersTable.tableBody(ctrl.players(),
                                                ctrl.model.activatedTeams(),
                                                ctrl.model.activatedScores(),
                                                ctrl.model.searchTerm()

                                            )
        );
    };

    var playersPage = {};

    playersPage.controller = function(){
        var model = {};
        model.activatedTeams = m.prop(_.map(teams.teams(), 'name'));
        model.activatedScores = m.prop([1, 2, 3, 4, 5]);
        model.sorting = m.prop([{key: 'team', direction: 1},  {key: 'score', direction: -1}, {key: 'name', direction: 1}]);
        model.searchTerm = m.prop('');
        return model;
    };

    playersPage.downloadButton = function(){
        return m('button.uk-button.uk-button-success.uk-border-rounded',
            m('i.fa.fa-download', {'aria-hidden': true}),
            " Télécharger");
    };

    playersPage.view = function(model){
        return  m('div.uk-grid.uk-margin-top.uk-margin-bottom',
                    m('div.uk-width-large-3-5.uk-width-small-1-1.uk-container-center', [
                        m.component(playersSelectionBar, model),
                        m.component(playersTable, model)
                    ]));
    };

    return playersPage;
});
