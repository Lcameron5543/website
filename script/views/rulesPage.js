define(['m', '_', 'util', 'resultParser', 'views/topNavBar', 'views/footer'], function (m, _ , util, resultParser, topNavBar, footer) {

    var rulesPage = {};

    rulesPage.missionText = function(){
        return "La ligue de balle Les Phénix a été fondée en 2004 afin "+
               "d'offrir aux membres de la communauté gaie et lesbienne et à "+
               "leurs amis hétéros l'opportunité de s'amuser et de pratiquer "+
               "un sport dans une ambiance amicale et décontractée. "+
               "L'ouverture d'esprit, la camaraderie et le plaisir de "+
               "pratiquer un sport à l'extérieur caractérisent cette ligue.";
    };

    rulesPage.logo = function(){
        var style = 'height: 250px; width: 250px; background-size: 250px; background-image: url(images/logosansfond.JPG);';
        return m('div.uk-cover-background.uk-container-center', {style: style});
    };

    rulesPage.ruleSection = function(){
        var urlASBM = 'http://www.asbm.ca/';
        return [
            m('h2', 'Règlements 2021'),
            m('ul',[
                m('li',  m('h3', m('a', {href: 'files/regle_de_jeux_2021.pdf'},'Règles de jeu'))),
                m('li',  m('h3', m('a', {href: 'files/regles_et_procedures_2021.pdf'},'Règles et procédures'))),
                m('li', m('h3', m('a', {href: 'files/charte_organisationnelle_2021.pdf'}, 'Charte Organisationnelle'))),
                m('li', m('h3', m('a', {href: 'files/formulaire_inscription_2021.pdf'}, 'Formulaire d\'inscription'))),
                m('li', m('h3', m('a', {href: 'files/code_ethique_2021.pdf'}, 'Code d\'éthique'))),
                m('li',  m('h3', m('a', {href: urlASBM}, 'Association des Sports de Balle à Montréal (ASBN)'))),
            ])
        ];
    };

    rulesPage.rules = function(){
        return  m('div.uk-width-large-2-3.uk-width-small-1-1',
                    m('div.uk-panel.uk-panel-box.uk-panel-box-secondary',
                        rulesPage.ruleSection()
                    )
                );
    };


    rulesPage.logoAndRules = function(){
        return  m('div.uk-grid',[
                    m('div.uk-width-large-1-3.uk-width-small-1-1', rulesPage.logo()),
                    rulesPage.rules()
                    ]
                );
    };

    rulesPage.mission = function(){
        return m('div.uk-panel.uk-panel-box.uk-panel-box-secondary', [
            m('h2', 'Mission'),
            m('p', rulesPage.missionText())
        ]);
    };

    rulesPage.view  = function(){
        return [
                m.component(topNavBar),

                m('div.uk-margin-top.uk-margin-bottom.uk-grid',
                m('div.uk-width-large-3-5.uk-width-medium-4-5.uk-width-large-1-1.uk-container-center', [
                    rulesPage.logoAndRules(),
                    rulesPage.mission()
                ])),
                m.component(footer)
            ];
    };

    return rulesPage;
});
