define(['m', '_', 'util', 'resultParser', 'teams', 'views/scheduleTeamTags', 'images/rain.svg'], function(m, _, util, resultParser, teams, scheduleTeamTags, rainSvg) {


    var schedulView = {};

    schedulView.controller = function(year){
        var games = m.prop([]);
        return util.getResults(year)
               .then(games)
               .then(resultParser.getNightGames);
    };


    schedulView._asGamePoints = function(points){
        return !_.isNumber(points) ? '-' : points + '';
    };

    schedulView._rainIcon = function(game){
        if(game.rain){
            return m('span', {title: 'pluie, partie annulée.'}, rainSvg('1.5em'));
        }
        return m('span', {style: {visibility: 'hidden'}}, rainSvg('1.5em'));
    };

    schedulView._gameName = function(game){
        return [schedulView._rainIcon(game),
                " ",
                game.time];
    };

    schedulView._gameRow = function(game){

        return m('tr',[
            m('td', schedulView._gameName(game)),
            m('td', scheduleTeamTags.getAwayTeamTag(game)),
            m('td', schedulView._asGamePoints(game.away_points)),
            m('td', 'vs'),
            m('td', scheduleTeamTags.getHomeTeamTag(game)),
            m('td', schedulView._asGamePoints(game.home_points))
        ]);
    };

    schedulView._gameView = function(gameNight){
        return m('div', [m('h3', util.formatDate(gameNight.jsDate)),
            m('table.uk-table.uk-table-hover.uk-table-condensed.uk-table-striped',
                m('tbody',[
                    _.map(gameNight.games, schedulView._gameRow),
                ])
            )
        ]);

    };

    schedulView.view = function(games){
        return m('div.uk-grid',
                   m('div.uk-width-large-3-5.uk-width-medium-1-1',
                    _.map(games(), schedulView._gameView)));
    };
    return schedulView;
})
