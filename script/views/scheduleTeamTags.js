define(['m', '_', 'teams'], function(m, _, teams) {


    var scheduleTeamTags = {};

    scheduleTeamTags._getTeamTag = function(teamName){
        var teamColor = teams.getTeamColor(teamName);
        return [
            m('i.fa.fa-flag', {style: {color: teamColor}}),
            m('span', ' ' + teamName)
        ];
    };

    scheduleTeamTags._getAwayPracticeTeamTag = function(){
        return  [
            m('i.fa.fa-hand-spock-o', {style: {color: ' #ff1493'}, title: 'pratique'}),
            m('span', ' Amis #1')
        ];
    };

    scheduleTeamTags._getHomePracticeTeamTag = function(){
        return  [
            m('i.fa.fa-hand-spock-o', {style: {color: '#acbf60'}, title: 'pratique'}),
            m('span', ' Amis #2')
        ];
    };

    scheduleTeamTags.getAwayTeamTag = function(game){
        if(game.practice){
            return scheduleTeamTags._getAwayPracticeTeamTag();
        }
        return scheduleTeamTags._getTeamTag(game.away);
    };

    scheduleTeamTags.getHomeTeamTag = function(game){
        if(game.practice){
            return scheduleTeamTags._getHomePracticeTeamTag();
        }
        return scheduleTeamTags._getTeamTag(game.home);
    };



    return scheduleTeamTags;
})
