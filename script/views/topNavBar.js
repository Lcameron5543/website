define(['m', '_', 'util'], function (m, _, util) {

    var topNavBar = {};

    topNavBar._urlMapping = [
        {name: 'Accueil', path: '', fa: 'fa-home'},
        {name: 'Nouvelles', path: 'nouvelles', fa: 'fa-newspaper-o'},
        {name: 'Calendrier', path: 'calendrier', fa: 'fa-calendar', includeSubPath: true},
        {name: 'Règlement', path: 'reglement', fa: 'fa-legal'},
        {name: 'Membre', path: 'membre', fa: 'fa-lock'},
        // {name: 'Historique', path: 'historique', fa: 'fa-history'},
        {name: 'Nous Joindre', path: 'contact', fa: 'fa-phone'}
    ];

    topNavBar.controller = function(){
        var model = {};
        model.openDropdown = false;
        return model;
    };

    topNavBar._getUrlForId = function(id_){
        return  '#/' + id_;
    };

    topNavBar._isActive = function(id_, includeSubPath){
        if(includeSubPath){
            return window.location.hash.startsWith(topNavBar._getUrlForId(id_));
        }
        return topNavBar._getUrlForId(id_) == window.location.hash;
    };
    topNavBar._getItemIcon = function(item){
        return m('i.fa.' + item.fa);
    };

    topNavBar._getLargeItem = function(item){
        var class_ = topNavBar._isActive(item.path, item.includeSubPath) ? 'uk-active' : '';
        var url = topNavBar._getUrlForId(item.path);
        return  m("li", {class: class_},
                    m("a", {href: url},
                        m('div', topNavBar._getItemIcon(item), ' ', item.name)
                    )
                );
    };

    topNavBar._getMediumItem = function(item){
        var class_ = topNavBar._isActive(item.path) ? 'uk-active' : '';
        var url = topNavBar._getUrlForId(item.path);
        return  m("li", {class: class_},
                    m("a", {href: url},
                        topNavBar._getItemIcon(item), ' ', item.name
                    )
                );
    };

    topNavBar._getSmallItem = function(item){
        var class_ = topNavBar._isActive(item.path) ? 'uk-active' : '';
        var url = topNavBar._getUrlForId(item.path);
        return  m("li", {class: class_},
                    m("a", {href: url},
                        topNavBar._getItemIcon(item),
                        ' ',
                        item.name
                    )
                );
    };

    topNavBar.largeTopNav = function(){
        return [
                m('li.uk-parent', m('div.uk-navbar-brand', util.facebookLink())),
                _.map(topNavBar._urlMapping, topNavBar._getLargeItem)
            ];
    };

    topNavBar.mediumTopNav = function(){
        return [
            m('li.uk-parent',  m('div.uk-navbar-brand', util.facebookLink())),
            _.map(topNavBar._urlMapping, topNavBar._getMediumItem)
        ];
    };

    topNavBar.dropdown = function(){
        return  m('div.uk-dropdown',
                    m('ul.uk-nav.uk-nav-navbar.uk-dropdown-navbar',
                        _.map(topNavBar._urlMapping, topNavBar._getSmallItem)
                ));
    };

    topNavBar.toggleDropdown = function(model){
        return function(){
            model.openDropdown = !model.openDropdown;
        };
    };

    topNavBar.clickElsewhere = function(model){
        var style = {
            position: 'fixed',
            top: '0px',
            bottom: '0px',
            left: '0px',
            right: '0px',
            background: 'black',
            opacity: 0.5,
            'z-index': 2
        };
        var attrs = {style: style, onclick: topNavBar.toggleDropdown(model)};
        return m('div', attrs);
    };

    topNavBar.smallTopNav = function(model){
        var class_ = model.openDropdown ? 'uk-open' : '';
        var attrs = {class: class_, onclick:  topNavBar.toggleDropdown(model)};
        var ret = [
            m('li.uk-parent',  m('div.uk-navbar-brand', util.facebookLink())),
            m('li.uk-parent[data-uk-dropdown][aria-expanded]', attrs, m('a', m('i.fa.fa-bars'), ' Menu'), topNavBar.dropdown()),
        ];
        if(model.openDropdown){
            ret.push(topNavBar.clickElsewhere(model));
        }
        return ret;
    };

    topNavBar.view = function(model){
        return  m("nav.uk-navbar.uk-navbar-attached", [
                    m("ul.uk-navbar-nav.uk-visible-large.uk-navbar-content.uk-navbar-center", topNavBar.largeTopNav()),
                    m("ul.uk-navbar-nav.uk-visible-medium", topNavBar.mediumTopNav()),
                    m("ul.uk-navbar-nav.uk-visible-small", topNavBar.smallTopNav(model)),
        ]);
    };

    return topNavBar;
});
